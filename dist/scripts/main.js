jQuery(function($){
  $(function(){
    /* Common */
    var lang='';
    var color;

    if($('body').hasClass('en'))
      lang = 'en';
    else
      lang = 'cn';


    $('.language-menu a').click(function(evt){
      evt.preventDefault();
      if(lang=='en'){
        window.location = window.location.pathname.replace('en/','')
      }else{
        window.location = '/en'+window.location.pathname;
      }
    });
    $('.flexslider').each(function(){$('.flexslider').flexslider({animation: 'slide',directionNav:false});});
    function getTemplate(templateUrl) {
      return $.ajax({
          type: 'GET',
          url: templateUrl,
          async: false
      }).responseText;
    }
    $('.contact-form').find('button[type=submit]').click(function(evt){
        evt.preventDefault();
        $.ajax({
            url:'/services',
            data:{
                name: $('#contactName').val(),
                email: $('#contactMail').val(),
                phone: $('#contactPhone').val(),
                message:$('#contactMessage').val()
            },
            success:function(d){
              $('#contactName').val('')
                $('#contactMail').val('')
                $('#contactPhone').val('')
                $('#contactMessage').val('')
            }
        })
    });

    $('.nav-tabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });
    /* End Common */

    var newYear = new Date();
    newYear = new Date(2017, 6, 25, 00, 00);

    var generalHtml = '';
    generalHtml = '<div class="fancybox-wrap modal-dialog general-dialog red" tabIndex="-1">';
    generalHtml += '<div class="fancybox-skin"><div class="fancybox-outer">';
    generalHtml += '<div class="fancybox-inner"></div>';
    generalHtml += '</div>';
    generalHtml += '<img src="/images/red-left-top.png" class="top-left" />';
    generalHtml += '<img src="/images/red-right-top.png" class="top-right" />';
    generalHtml += '<img src="/images/red-left-bottom.png" class="bottom-left" />';
    generalHtml += '<img src="/images/red-right-bottom.png" class="bottom-right" />';
    generalHtml += '</div></div>';
    $('#launch-avatar').fancybox({
      title:null,
      padding:0,
      minWidth:960,
      autoSize:false,
      tpl:{
        wrap: generalHtml
      },
      beforeShow:function(){
        var $inputImage = $('#inputImage');
        var uploadedImageURL;
        var $image = $('.uploaded');
        var URL = window.URL || window.webkitURL;
        var $previews = $('.preview');
        var options = {
          zoomOnWheel:false,
          responsive:false,
          viewMode:1,
          aspectRatio: 1 / 1,
          build: function (e) {
				  var $clone = $(this).clone();

          $clone.css({
            display: 'block',
            width: '100%',
            minWidth: 0,
            minHeight: 0,
            maxWidth: 'none',
            maxHeight: 'none'
          });

          $previews.css({
            width: '100%',
            overflow: 'hidden'
          }).html($clone);
			   },
			   crop: function (e) {
				   var imageData = $(this).cropper('getImageData');
				   var previewAspectRatio = e.width / e.height;

				   $previews.each(function (){
					   var $preview = $(this);
					   var previewWidth = $preview.width();
					   var previewHeight = previewWidth / previewAspectRatio;
					   var imageScaledRatio = e.width / previewWidth;

					   $preview.height(previewHeight).find('img').css({
						   width: imageData.naturalWidth / imageScaledRatio,
						   height: imageData.naturalHeight / imageScaledRatio,
						   marginLeft: -e.x / imageScaledRatio,
						   marginTop: -e.y / imageScaledRatio
					   });
				   });
			   }
        }
        $image.cropper(options);

        if (URL) {
          $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
              return;
            }

            if (files && files.length) {
              file = files[0];

              if (/^image\/\w+$/.test(file.type)) {
                if (uploadedImageURL) {
                  URL.revokeObjectURL(uploadedImageURL);
                }

                uploadedImageURL = URL.createObjectURL(file);
                $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                $inputImage.val('');
              } else {
                window.alert('Please choose an image file.');
              }
            }
          });
        } else {
          $inputImage.prop('disabled', true).parent().addClass('disabled');
        }

        $('#pick-avatar .avatar').click(function(evt){
          evt.preventDefault();
          $('.avatar').removeClass('selected');
          $(this).addClass('selected');
          var $clone = $(this).find('img').clone();

          $clone.css({
            display: 'block',
            width: '100%',
            minWidth: 0,
            minHeight: 0,
            maxWidth: 'none',
            maxHeight: 'none'
          });

          $previews.css({
            width: '100%',
            overflow: 'hidden'
          }).html($clone);
        });
      }
    });


    if($('.loggedin').length>0){
      var clipboard = new Clipboard('.copy-me-btn');

  		clipboard.on('success', function(e) {
  			var clicked = e.trigger.id;
  			e.clearSelection();
  			return false;
  		});
    }

    /* Index */
    if($('.all-game').length>=1){

      $.ajax({
        url:'/json/games.json',
        type:'GET',
        dataType: 'json'
      }).done(function(d){
        var gameTableTemplate = getTemplate('/template/all-game-'+lang+'.html');
        var dotted = doT.template(gameTableTemplate);
        var games = d.payload.games;
        $(games).each(function(i){
          var timestamp = games[i].timestamp;
          games[i].timestamp = moment.unix(timestamp).format('MM/DD/YYYY');
          games[i].utc = timestamp;
        });
        $('.all-game').html(dotted(games));
        var current = moment().unix();

        $('.bet-countdown').each(function(){
          var gameTimeStamp = $(this).text();
          if (current >= gameTimeStamp) {
            if($('body').hasClass('en')){
              $(this).html('<div class="counting-done">Live Stream</div>');
            }else{
              $(this).html('<div class="counting-done">查看直播</div>');
            }
          }else{
            var elapseDate = new Date(gameTimeStamp*1000);
            $self = $(this)

            function checkTime(el, t){
              if(el.find('span').eq(0).find('.number').text()==0){
                el.find('span').eq(0).hide()
              }
            }
            if(lang=='en'){
              $(this).countdown({
                until:elapseDate,
                expiryText: '<span class="warning">Live Stream</span>',
                layout: '<div class="counting"><span class="day"><span class="number">{dn}</span> d</span> <span class="hr">{hn}</span> hr</span> <span class="min">{mn}</span> min</span> {sn}</span> <span class="sec">sec</span> Left</div>',
                onTick: function(d){
                  checkTime($self,d);
                }
              });
            }else{
              $(this).countdown({
                until:elapseDate,
                expiryText: '<span class="warning">查看直播</span>',
                layout: '<div class="counting"><span class="day">剩<span class="number">{dn}</span>天</span><span class="hr"><span class="number">{hn}</span>小时</span><span class="min"><span class="number">{mn}</span>分钟</span><span class="sec"><span class="number">{sn}</span>秒</span></div>',
                onTick: function(d){
                  checkTime($self,d);
                }
              });
            }
          }
        });
      });

      $('.all-game').on('click','.show-chart',function(evt){
        evt.preventDefault();
        var id = $(this).attr('href').replace('#/','');

        $.ajax({
          url:'/json/'+id+'.json',
          type:'GET',
          dataType: 'json'
        }).done(function(d){
          var comparisonTemplate = getTemplate('/template/comparison-'+lang+'.html');
          var dotted = doT.template(comparisonTemplate);
          var comparison = d.payload;
          var html = dotted(comparison);
          var homeRadarkey = [],homeRadarValue=[],awayRadarkey=[],awayRadarValue=[],homeRadardata = comparison.home.radarchart,awayRadardata = comparison.away.radarchart;
          for(var homeK in homeRadardata){
            homeRadarkey.push({'name':homeK,'max':10});
            homeRadarValue.push(homeRadardata[homeK]);
          }
          for(var awayK in awayRadardata){
            awayRadarkey.push({'name':awayK,'max':10});
            awayRadarValue.push(awayRadardata[awayK]);
          }
          homeOption = {
            radar: {indicator: homeRadarkey},
            series: [{type: 'radar',itemStyle: {normal: {areaStyle: {type: 'default'}}},data : [{value : homeRadarValue}]}]
          };
          awayOption = {
            radar: {indicator: awayRadarkey},
            series: [{
              type: 'radar',
              itemStyle: {
                normal: {
                  areaStyle: {color:'#0064d4'},
                   lineStyle:{color:'#0064d4'}
                 }
               },
               data : [{
                 value : awayRadarValue,
                 itemStyle:{
                   normal:{color:'#0064d4'}
                 }
               }]
             }]
          };
          wrapHtml = '<div class="fancybox-wrap '+comparison.color+'" tabIndex="-1">';
          wrapHtml += '<div class="fancybox-skin"><div class="fancybox-outer">';
          wrapHtml += '<div class="fancybox-inner"></div>';
          wrapHtml += '</div>';
          wrapHtml += '<img src="/images/'+comparison.color+'-left-top.png" class="top-left" />';
          wrapHtml += '<img src="/images/'+comparison.color+'-right-top.png" class="top-right" />';
          wrapHtml += '<img src="/images/'+comparison.color+'-left-bottom.png" class="bottom-left" />';
          wrapHtml += '<img src="/images/'+comparison.color+'-right-bottom.png" class="bottom-right" />';
          wrapHtml += '</div></div>';
          $.fancybox({
            type: 'html',
            content: html,
            minWidth: 1180,
            minHeight: 640,
            padding:[10, 30, 10, 30],
            tpl:{
            wrap: wrapHtml
            },
            afterShow: function(){
              $.fancybox.inner.jScrollPane();
              var homeChart = echarts.init(document.getElementById('radar-home'));
              homeChart.setOption(homeOption);
              var awayChart = echarts.init(document.getElementById('radar-away'));
              awayChart.setOption(awayOption);
            }
          });
        });

      });


      $('.modal').fancybox({
        title:null,
        padding:0,
        minWidth:600,
        tpl:{
          wrap: '<div class="fancybox-wrap modal-dialog" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
        },
        beforeShow: function(){
          $('.login-form').show();
          $('.recovery-password').hide();
          $('.change-password').hide();
        }
      });

      var generalHtml = '';
      generalHtml = '<div class="fancybox-wrap modal-dialog general-dialog red" tabIndex="-1">';
      generalHtml += '<div class="fancybox-skin"><div class="fancybox-outer">';
      generalHtml += '<div class="fancybox-inner"></div>';
      generalHtml += '</div>';
      generalHtml += '<img src="/images/red-left-top.png" class="top-left" />';
      generalHtml += '<img src="/images/red-right-top.png" class="top-right" />';
      generalHtml += '<img src="/images/red-left-bottom.png" class="bottom-left" />';
      generalHtml += '<img src="/images/red-right-bottom.png" class="bottom-right" />';
      generalHtml += '</div></div>';

      $('.modal-btn').fancybox({
        title:null,
        padding:0,
        minWidth:1000,
        tpl:{
          wrap: generalHtml
        },
      });
      $('.go-register-success').click(function(evt){
        evt.preventDefault();
        $.fancybox.close();
        $('#go-register-success').click();
      });
      //$('#go-login-success-inactive').click();
      $('.login-form').submit(function(e){
        e.preventDefault();
        $.fancybox.close();
        $('#go-login-success').click();
      });
      $.validator.addMethod("regex", function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }, function(params, el) {
            return "不允许出现符号";
        });


      $('.register-form').submit(function(e) {
            e.preventDefault()
        }).validate({
          rules:{
            regLogin:{
              required:true,
              regex: "^[a-zA-Z0-9]*$"
            },
            regPassword: {
                required: true,
                minlength: 8
            }
          },
          messages: {
            regLogin:{
              required:'比填',
            },
            regPassword: {
              required:'比填',
              minlength: '密码长度最少为8位数',
            }
          },
          submitHandler: function(form) {
            console.log(form);
            return false;
          }
      });
      if ($('.checkpassword').length > 0) {
           $('.checkpassword').strength();
      }

      $('.go-register').click(function(evt){
        evt.preventDefault();
        $.fancybox.close();
        setTimeout(function(){
          $('#launch-register').click();
        },300);
      });

      $('.forget').click(function(evt){
        evt.preventDefault();
        $('.login-form').hide();
        $('.recovery-password').show();
      });

      function countDown(i){
          var int = setInterval(function () {
            $('.timer').text(i+'s');
            i-- || clearInterval(int);
          }, 1000);
      }
      $('.recovery-password').on('click','.chrome', function(evt){
        evt.preventDefault();
        $('.chrome').hide();
        $('.verification').css({'display':'inline-block'});
        countDown(60);
      });
      $('.go-change-pw').click(function(evt){
        evt.preventDefault();
        $('.change-password').show();
        $('.recovery-password').hide();
      });
    }

    $('#accordion').on('show.bs.collapse', function () {
        $(this).find('.arrow').addClass('open').removeClass('closed');
    });
    $('#accordion').on('hidden.bs.collapse', function () {
         $(this).find('.arrow').addClass('closed').removeClass('open');
    });

    if(lang=='en'){
      $("input#market").bootstrapSwitch({
        onText:'Euro',
        offText:'Asian',
        handleWidth:60,
        labelWidth:20
      });
    }else{
      $("input#market").bootstrapSwitch({
        onText:'欧洲赔率',
        offText:'亚洲赔率',
        handleWidth:60,
        labelWidth:20
      });
    }

    /* End Index */

    var noScrollHtml = '';
    noScrollHtml = '<div class="fancybox-wrap modal-dialog general-dialog noscroll red" tabIndex="-1">';
    noScrollHtml += '<div class="fancybox-skin"><div class="fancybox-outer">';
    noScrollHtml += '<div class="fancybox-inner"></div>';
    noScrollHtml += '</div>';
    noScrollHtml += '<img src="/images/red-left-top.png" class="top-left" />';
    noScrollHtml += '<img src="/images/red-right-top.png" class="top-right" />';
    noScrollHtml += '<img src="/images/red-left-bottom.png" class="bottom-left" />';
    noScrollHtml += '<img src="/images/red-right-bottom.png" class="bottom-right" />';
    noScrollHtml += '</div></div>';


    $('.launch-dota').fancybox({
      title:null,
      padding:0,
      minWidth:960,
      tpl:{
        wrap: noScrollHtml
      }
    });
    $('.launch-lol').fancybox({
      title:null,
      padding:0,
      minWidth:960,
      tpl:{
        wrap: noScrollHtml
      }
    });
    $('.launch-refer-modal').fancybox({
      title:null,
      padding:0,
      minWidth:980,
      maxWidth:980,
      minHeight:550,
      tpl:{
        wrap: noScrollHtml
      }
    });
    $('.enter-modal').fancybox({
      title:null,
      padding:0,
      minWidth:980,
      maxWidth:980,
      minHeight:550,
      tpl:{
        wrap: generalHtml
      }
    });

    $('#topup-modal').on('click','.submit-pass',function(evt){

      evt.preventDefault();
      $('#topup-modal .modal-inner').hide();
      $('#topup-modal .success-message').show();
        $(window).resize();
    });


    /* Bet */

    setInterval(function() {
      $('.question-selection').each(function(el){
          var status=['raising','','dropping'];
          var statusimg=['up','','down'];
          var random_num =  Math.floor(Math.random()*2) == 1 ? 1 : -1;

          var self = $(this);
          self.find('.odd-tracker').addClass('blink');
          setTimeout(function(){
               self.find('.odd-tracker').removeClass('blink raising dropping');
               self.find('.odd-tracker').first().not('.idle').addClass(status[(random_num)+1])
               self.find('.odd-tracker').eq(1).not('.idle').addClass(status[(random_num*-1)+1])
               self.find('.odd-tracker').first().not('.idle').find('.odd img').attr('src', '/images/'+statusimg[(random_num)+1]+'.png');
               self.find('.odd-tracker').eq(1).not('.idle').find('.odd img').attr('src', '/images/'+statusimg[(random_num*-1)+1]+'.png');
           }, 250);

      });
    }, 5000);


    if($('.road-map').length>=1){
      $.ajax({
        url:'/json/roadmap-'+lang+'.json',
        type:'GET',
        dataType: 'json'
      }).done(function(d){
        var roadmapTemplate = getTemplate('/template/roadmap-'+lang+'.html');
        var dotted = doT.template(roadmapTemplate);
        var roadmap = d.payload.roadmap;
        $('.road-map .panel-group').html(dotted(roadmap));
        $('.roadmap-table').roadmapTable();
      });
    }
    if($('#bet-modal').length>0){
      color = $('#bet-modal').attr('class');
      var wrapHtml = '';
      wrapHtml = '<div class="fancybox-wrap modal-dialog bet-dialog '+color+'" tabIndex="-1">';
      wrapHtml += '<div class="fancybox-skin"><div class="fancybox-outer">';
      wrapHtml += '<div class="fancybox-inner"></div>';
      wrapHtml += '</div>';
      wrapHtml += '<img src="/images/'+color+'-left-top.png" class="top-left" />';
      wrapHtml += '<img src="/images/'+color+'-right-top.png" class="top-right" />';
      wrapHtml += '<img src="/images/'+color+'-left-bottom.png" class="bottom-left" />';
      wrapHtml += '<img src="/images/'+color+'-right-bottom.png" class="bottom-right" />';
      wrapHtml += '</div></div>';
    }
    $('.close-modal').click(function(evt){
      evt.preventDefault();
      $.fancybox.close();
    });
    $('#money').bootstrapToggle();
    $('.pass-checkbox').bootstrapToggle();
    if($('.bet-list').length>=1){
      $.ajax({
        url:'/json/bet-list-'+lang+'.json',
        type:'GET',
        dataType: 'json'
      }).done(function(d){
        var oddTableTemplate = getTemplate('/template/odd-table-'+lang+'.html');
        var dotted = doT.template(oddTableTemplate);
        var odd = d.payload.oddlist;
        odd.color = color;
        $('.tab-content .all-odd').html(dotted(odd))

        $('.place-bet').TouchSpin({
            min: 0,
            prefix: '$'
        });
        $('.bet-modal').fancybox({
          title:null,
          padding:0,
          minWidth:980,
          maxWidth:980,
          tpl:{
            wrap: wrapHtml
          },
          afterClose:function(){
            $('.success-message').hide();
            $('.modal-inner').show();
          }
        });

        $('.bet-history-modal').fancybox({
          title:null,
          padding:0,
          minWidth:890,
          maxWidth:890,
          maxHeight:700,
          tpl:{
            wrap: wrapHtml
          },
          afterShow: function(){
             $.fancybox.inner.jScrollPane({
               verticalDragMinHeight: 300,
                verticalDragMaxHeight: 500,
                autoReinitialise :true
             });
          }
        });

        $('.pass-modal').fancybox({
          title:null,
          padding:0,
          minWidth:950,
          maxWidth:950,
          tpl:{
            wrap: wrapHtml
          },
          afterClose:function(){
            $('.success-message').hide();
            $('.modal-inner').show();
          }
        });

        $('.bet-check input').simpleImageCheck({
          image: '/images/'+color+'-border.png',
          imageChecked: '/images/tick.png'
        });

        $('.coins a').click(function(evt){
          evt.preventDefault();
          $listen = $(this).parents('.modal-body').find('#bet-amount')
          oldVal = ($listen.val()!='undefined')?parseInt($listen.val()):0;
          newVal = parseInt($(this).attr('data-plus'));
          $listen.val(newVal+oldVal)
          var audio = document.getElementById("coin");
          audio.play();
        })
      });
    }
    $('.bet-list').on('click','.add-to-pass',function(evt){
      evt.preventDefault();
      $(".pass-modal").tooltip('show');
      setTimeout(function(){
        $(".pass-modal").tooltip('hide');
      },2000)
    })
    $('.bet-list').on('click','.add-parlay',function(evt){
      evt.preventDefault();
      $(".pass-modal").tooltip('show');
      setTimeout(function(){
        $(".pass-modal").tooltip('hide');
      },2000)
    });
    $('#bet-modal').on('click','.submit-bet',function(evt){
      evt.preventDefault();
      $('#bet-modal .modal-inner').hide();
      $('#bet-modal .success-message').show();
      $(window).resize();
    });
    $('#pass-modal').on('click','.submit-pass',function(evt){
      evt.preventDefault();
      $('#pass-modal .modal-inner').hide();
      $('#pass-modal .success-message').show();
        $(window).resize();
    });

    $('.sticky ul').jScrollPane({
      height: 400,
      autoReinitialise: true,
      verticalDragMaxHeight: 50
    });

    $('.window-toggle').click(function(evt){
      evt.preventDefault();
      $(this).find('img').toggle();
      $('.sticky-content').toggle()
      $('.sticky-title span').toggle()
    });
    $('.toggle-sticky').click(function(evt){
      evt.preventDefault();
      $('.sticky').hide();
      $('.parlay-li').find('span').show();
    });
    $('.parlay-li').find('span').click(function(evt){
      evt.preventDefault();
      $('.sticky').show();
      $(this).hide();
    });
    if(lang=='en'){
      $('.countdown').countdown({
        until: newYear,
        compact:true,
        padZeroes: true,format:'0DHMS',
        layout: '<div class="days">{dn}<div class="unit">D</div></div><div class="hours">{hn}<div class="unit">H</div></div><div class="minutes">{mn}<div class="unit">M</div></div><div class="seconds">{sn}<div class="unit">S</div></</div>'
      });
    }else{
      $('.countdown').countdown({
        until: newYear,
        compact:true,
        padZeroes: true,format:'0DHMS',
        layout: '<div class="days">{dn}<div class="unit">天</div></div><div class="hours">{hn}<div class="unit">小时</div></div><div class="minutes">{mn}<div class="unit">分</div></div><div class="seconds">{sn}<div class="unit">秒</div></</div>'
      });
    }
    /* End Bet*/

    /* Personal Center */

    $('.leader-flexslider').flexslider({
      animation: "slide",
      itemWidth: 231,
      itemMargin: 8,
      maxItems: 3,
      directionNav:true,
      controlNav:false,
      before:function(f){
        f.resize();
      },
      after:function(f){
        f.resize();
      }
    });

    if (window.location.hash) {
        if ((window.location.hash).match(/leaderboard/g)) {
          setTimeout(function(){
            $('.leaderboard-tab').click();
          },500);

        }
    }
    $(".rating").starRating({
          starSize: 16,readOnly:true,strokeColor: '#dddddd',emptyColor: 'transparent',hoverColor:'#f1db80',
          starGradient: {start: '#F6E8AE',  end: '#EACD76'}
    });

    $('.milestone-wrap').jScrollPane({
      height: 300,
      autoReinitialise: true
    });

    $('.mission-wrap').jScrollPane({
      height: 600,
      autoReinitialise: true
    });

    $('#detailDOB').datetimepicker({
        format: 'DD.M.YYYY',
        useCurrent: false
    });

    $('.next-state').click(function(evt){
      evt.preventDefault();
      $(this).hide();
      var box = $(this).parents('.tab-box')
      box.find('.done-state').show();
      box.find('.original-state').hide();
    });
    $('.claim-btn').click(function(evt){
      evt.preventDefault();
      $(this).hide();
      var box = $(this).parents('.tab-box')
      box.find('.done-state').hide();
      box.find('.claim-state').show();
      box.find('.claim').hide();
      box.find('.chrome').hide();
    });
    $('.view-more .more').click(function(evt){
      evt.preventDefault();
      $('.hidden-list').show();
      $('.view-more').hide();
    });
    $(".verification").click(function(evt){
      evt.preventDefault();
        $(this).tooltip('show');
        setTimeout(function(){
            $(".verification").tooltip('hide');
        },200000)
    });
    $('.edit-btn').click(function(evt){
      evt.preventDefault();
      $(this).hide();
      var box = $(this).parents('.tab-box')
      box.find('.done-state').hide();
      box.find('.claim-state').hide();
      box.find('.final-state').show();
      box.find('.confirm-state').show();
    });
    $('.final-state').click(function(evt){
      evt.preventDefault();
      $(this).hide();
      var box = $(this).parents('.tab-box');
      box.find('.confirm-state').hide();
      box.find('.claim-state').show();
      box.find('.edit-btn').show();
    });
    if($('.missing-reminder').length>0){
      $('.enter-modal').click();
    }
    $('.toggle-parlay').click(function(evt){
      evt.preventDefault();
      var childClass  = $(this).attr('id');
      $(this).toggleClass('parlay-down');
      $(this).toggleClass('parlay-up');
      $('.'+childClass).find('.slide').slideToggle();
    });

    $('#selectDate').datetimepicker({
        format: 'DD.M.YYYY'
    });


    if(lang=='cn'){
      if($('#week-chart').length>0){
        var weekOption= {
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ '號: ' + params.data;
            }
          },
          xAxis:[
            {
              type : 'category',
              position: 'bottom',
              boundaryGap: true,
              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}號',
                textStyle:{color:'#fff'},
              },
              data : [  '28','29','30','31','1','2','3' ]
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff'}
              }
            }
          ],
          series:[
            {
              type: 'line',
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3, 0, 0, 7, 2]
            }
          ]
        };

        var monthOption= {
          grid:{
            x: 50,
            y: 60,
            x2: 10,
            y2: 90
          },
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ '號: ' + params.data;
            }
          },

          xAxis:[
            {
              type : 'category',
              position: 'bottom',

              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}號',
                textStyle:{color:'#fff',fontSize:8},
                margin: 10,

              },
              data : [ '28','29','30','31','1','2','3','4', '5', '6', '7', '8', '9', '10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27']
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff',fontSize:12}
              }
            }
          ],
          series:[
            {
              type: 'line',
              boundaryGap: true,
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3, 0, 0, 7, 2,8, 3, 2, 9, 7, 2,22, 4, 1, 1, 0 ,2, 0, 2, 1, 1, 1, 2, 0 ,0, 0, 0 ,1,2]
            }
          ]
        };
        var halfYearOption= {
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ '月: ' + params.data;
            }
          },
          xAxis:[
            {
              type : 'category',
              position: 'bottom',
              boundaryGap: true,
              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}月',
                textStyle:{color:'#fff'},
              },
              data : [  '1','2','3','4','5','6' ]
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff'}
              }
            }
          ],
          series:[
            {
              type: 'line',
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3,9,100,5]
            }
          ]
        };

        var weekChart = echarts.init(document.getElementById('week-chart'));
        weekChart.setOption(weekOption);
        var monthChart = echarts.init(document.getElementById('month-chart'));
        monthChart.setOption(monthOption);
        var halfYearChart = echarts.init(document.getElementById('halfyear-chart'));
        halfYearChart.setOption(halfYearOption);
      }

    }else{

      if($('#week-chart').length>0){
        var weekOption= {
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ ': ' + params.data;
            }
          },
          xAxis:[
            {
              type : 'category',
              position: 'bottom',
              boundaryGap: true,
              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}',
                textStyle:{color:'#fff'},
              },
              data : [  '28','29','30','31','1','2','3' ]
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff'}
              }
            }
          ],
          series:[
            {
              type: 'line',
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3, 0, 0, 7, 2]
            }
          ]
        };

        var monthOption= {
          grid:{
            x: 50,
            y: 60,
            x2: 10,
            y2: 90
          },
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ ': ' + params.data;
            }
          },

          xAxis:[
            {
              type : 'category',
              position: 'bottom',

              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}',
                textStyle:{color:'#fff',fontSize:8},
                margin: 10,

              },
              data : [ '28','29','30','31','1','2','3','4', '5', '6', '7', '8', '9', '10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27']
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff',fontSize:12}
              }
            }
          ],
          series:[
            {
              type: 'line',
              boundaryGap: true,
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3, 0, 0, 7, 2,8, 3, 2, 9, 7, 2,22, 4, 1, 1, 0 ,2, 0, 2, 1, 1, 1, 2, 0 ,0, 0, 0 ,1,2]
            }
          ]
        };
        var halfYearOption= {
          tooltip:{
            trigger: 'item',
            formatter: function (params) {
              return params.name+ 'Month: ' + params.data;
            }
          },
          xAxis:[
            {
              type : 'category',
              position: 'bottom',
              boundaryGap: true,
              splitLine:{
                show:true,
                lineStyle:{
                  color: ['#272929']
                }
              },
              axisLabel:{
                show:true,
                formatter: '{value}月',
                textStyle:{color:'#fff'},
              },
              data : [  '1','2','3','4','5','6' ]
            }
          ],
          yAxis:[
            {
              type : 'value',
              min: 0,
              max: 1000,
              type: 'value',
              splitLine:{show:false},
              axisLine: {onZero: true},
              axisLabel:{
                textStyle:{color:'#fff'}
              }
            }
          ],
          series:[
            {
              type: 'line',
              itemStyle: {
                normal: {
                   lineStyle:{color:'#f8e3a2'},
                   borderWidth:5,
                   borderColor:'#f8e3a2'
                 }
               },
              data: [20, 17, 3,9,100,5]
            }
          ]
        };

        var weekChart = echarts.init(document.getElementById('week-chart'));
        weekChart.setOption(weekOption);
        var monthChart = echarts.init(document.getElementById('month-chart'));
        monthChart.setOption(monthOption);
        var halfYearChart = echarts.init(document.getElementById('halfyear-chart'));
        halfYearChart.setOption(halfYearOption);
      }

    }

    /* End Personal Center */

    /* Leaderboard */
    $('.go-leaderboard').click(function(evt){
      evt.preventDefault();
      $('.leaderboard-tab').trigger('click');
    });
    $('.poll .scrollWrap').jScrollPane({
      height: 400,
      autoReinitialise: true
    });
    $('.leaderboard-tab-inner-content #oneweek-player .scrollWrap').jScrollPane({
      autoReinitialise: true
    });
    if(lang=='cn'){
      $('.cn .leaderboard-countdown').countdown({
        until: newYear,
        compact:true,
        padZeroes: true,format:'0DHMS',
        layout: '<div class="days">{dn}<div class="unit">天</div></div><div class="hours">{hn}<div class="unit">小时</div></div><div class="minutes">{mn}<div class="unit">分</div></div><div class="seconds">{sn}<div class="unit">秒</div></</div>'
      });
    }else{
      $('.en .leaderboard-countdown').countdown({
        until: newYear,
        compact:true,
        padZeroes: true,format:'0DHMS',
        layout: '<div class="days">{dn}<div class="unit">D</div></div><div class="hours">{hn}<div class="unit">H</div></div><div class="minutes">{mn}<div class="unit">M</div></div><div class="seconds">{sn}<div class="unit">S</div></</div>'
      });
    }

    /* End Leaderboard */
  });
  $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

      if(typeof halfYearChart != 'undefined'){
        halfYearChart.resize();
      }
      if(typeof weekChart != 'undefined'){
        weekChart.resize();
      }
      if(typeof monthChart != 'undefined'){
        monthChart.resize();
      }
  });
});
