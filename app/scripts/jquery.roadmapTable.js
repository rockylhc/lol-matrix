(function($){
    $.roadmapTable = function(el, init, options){
        var base = this;
        base.$el = $(el);
        base.el = el;
        base.$el.data('roadmapTable', base);

        base.init = function(data){
            base.options = $.extend({},$.roadmapTable.defaultOptions, options);
            var tdText = base.$el.find('td').text();
            var data;

            if (typeof(tdText)==null) return;
            if(tdText.length<=base.options.mapSize+3){
              var diff = base.options.mapSize-tdText.length+3;
              for (var f=0;f<diff;f++){
                tdText+='0'
              }
              data=tdText;
            }else{
              data=tdText.substring(0,base.options.mapSize+3);
            }
            var roundCount = data.substring(0,3);
            var gameHistory = data.substring(3);

            if(gameHistory.length<=0) return;
            var vArray = [];
            var re = new RegExp('\.{'+base.options.vertical+'\}','gi');

            var patternArray = gameHistory.match(re);

            var hArray = [];
            for (var i=0;i<patternArray.length;i++){
              patternArray[i] = patternArray[i].split('');
            }
            var matrix = base.transpose(patternArray);

            cssClass = ['','red empty','blue empty','red checked','blue checked','red cross','blue cross']
            //clean up DOM
            base.$el.find('tr').html('');
            var htmlTable = '';
            for(var ve=0;ve<base.options.vertical;ve++){

              htmlTable+='<tr>';
              for(var ho=0;ho<base.options.horizontal;ho++){

                htmlTable+='<td><span class="roadmap-icon '+cssClass[matrix[ve][ho]]+'"></span></td>';
              }
              htmlTable+='</tr>';
            }
            base.$el.html(htmlTable);
        };
        base.map = function(_x) {
            for(var o=[], i=0; i<this.length; i++) {
                o[i] = _x(this[i]);
            }
            return o;
        };
        base.transpose= function(a) {
          if(typeof(a)===undefined) return;
          var w=a.length?a.length:0,
          h=a[0] instanceof Array ? a[0].length:0;
          if(h===0||w===0){return [];}
          var i,j,t=[];
          for(i=0; i<h; i++) {
            t[i] = [];
            for(j=0; j<w; j++) {
              t[i][j] = a[j][i];
            }
          }
          return t;
        };
        base.init();
    };

    $.roadmapTable.defaultOptions = {
        horizontal: 13,
        vertical: 5,
        data:null,
        mapSize:65
    };

    $.fn.roadmapTable = function(init, options){
        return this.each(function(){
            (new $.roadmapTable(this, init, options));
        });
    };

})(jQuery);
